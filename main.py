import falcon
import json
from wsgiref import simple_server
from logzero import logger as log
from pprint import pformat
from uuid import uuid4


def pretty_log(media):
    lines = pformat(media).split("\n")
    for l in lines:
        log.info(l)


# Falcon follows the REST architectural style, meaning (among
# other things) that you think in terms of resources and state
# transitions, which map to HTTP verbs.
class EchoResource:
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.media = {
            "params": req.params,
        }
        new_event = uuid4()
        log.info("**************** EVENT %s ****************", new_event)
        log.info("%s %s %s", req.remote_addr, req.method, req.uri)
        log.info("**************** HEADERS ****************")
        log.info(req.headers)
        log.info("**************** STATUS_CODE ****************")
        log.info(resp.status)
        log.info("**************** MEDIA ****************")
        pretty_log(resp.media)
        log.info("**************** END %s ****************", new_event)

    def on_post(self, req, resp):
        """Handles GET requests"""
        if req.content_type is None:
            raise falcon.HTTPError(
                falcon.HTTP_400,
                    "This API only supports requests encoded as JSON."    # noqa
            )
        if "application/json" not in req.content_type:
            raise falcon.HTTPError(
                falcon.HTTP_400,
                    "This API only supports requests encoded as JSON."    # noqa
            )
        try:
            body = json.loads(req.bounded_stream.read())
            req.context.body = body
        except Exception:
            raise falcon.HTTPError(
                falcon.HTTP_400,
                "This API only supports requests encoded as JSON."    # noqa
            )
        resp.status = falcon.HTTP_200  # This is the default status
        resp.media = {
            "headers": req.headers,
            "params": req.params,
            "body": body,
        }
        new_event = uuid4()
        log.info("**************** EVENT %s ****************", new_event)
        log.info("%s %s %s", req.remote_addr, req.method, req.uri)
        log.info("**************** HEADERS ****************")
        log.info(req.headers)
        log.info("**************** STATUS_CODE ****************")
        log.info(resp.status)
        log.info("**************** MEDIA ****************")
        pretty_log(resp.media)
        log.info("**************** END %s ****************", new_event)


# falcon.App instances are callable WSGI apps
# in larger applications the app is created in a separate file
app = falcon.API()

# Resources are represented by long-lived class instances
things = EchoResource()

# things will handle all requests to the '/things' URL path
app.add_route('/echo', things)

if __name__ == "__main__":
    httpd = simple_server.make_server('127.0.0.1', 5000, app)
    httpd.serve_forever()
