FROM python:3.7.3-slim
MAINTAINER chuducminh@gapo.com.vn

EXPOSE 5000
WORKDIR /app
RUN apt-get update && apt-get install -y gunicorn gettext-base && pip install pipenv

ADD Pipfile Pipfile.lock /app/
RUN pipenv install --system --deploy

ADD . /app

ENTRYPOINT ["/app/entrypoint.sh"]

CMD ["gunicorn", "--chdir", "/app", "--log-level=info", "--timeout", "3600", "--keep-alive", "600", "-w", "4", "-b", "0.0.0.0:5000", "main:app"]